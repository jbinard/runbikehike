/* globals Backbone */
/* exported Message */
'use strict';

var Item = Backbone.Model.extend({
    idAttribute: '_id',
    save: false
});
